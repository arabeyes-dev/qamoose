#!/usr/bin/perl -T
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Dictionary's administration page.
#
#  + Approve/Reject new terms
#  + Approve/Reject new term-approvers
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - see COPYING file)
#---

# Let's make perl happy in terms of taint'ness :-)
$ENV{"PATH"}	= undef;

require DBI;

require "./dict_header.pl";
require "./dict_lib.pl";

use English;

my ($ref_in,
    $uploaded_file)	= &readform();

my %in			= %{$ref_in};
 
# Enable autoflushing
#STDOUT->autoflush(1);

# Don't cache any of these pages
print ("Cache-Control: no-cache\nPragma: no-cache\n");

# In some instances, I care about that first line of
# the HTML page - I want to give it a non-standard
# directive.
if ( $in{'action'} ne "logout"	&&
     $in{'action'} ne "login"	&&
     $in{'action'} ne "download_file"
   )
{ &do_html_header(); }

if	( $in{'action'} eq "login" )		{ &login; }
elsif	( $in{'action'} eq "logout" )		{ &logout; }
elsif	( $in{'action'} eq "do_getpass" )	{ &do_getpass; }
elsif	( $in{'action'} eq "getpass" )		{ &getpass; }
elsif	( $in{'action'} eq "do_apply" )		{ &do_apply; }
elsif	( $in{'action'} eq "apply" )		{ &apply; }
elsif	( $in{'action'} eq "do_pend_ulist" )	{ &do_pend_ulist; }
elsif	( $in{'action'} eq "do_user_resolve" )	{ &do_user_resolve; }
elsif	( $in{'action'} eq "do_term_resolve" )	{ &do_term_resolve; }
elsif	( $in{'action'} eq "do_search_local" )	{ &do_search_local; }
elsif	( $in{'action'} eq "search_local" )	{ &search_local; }
elsif	( $in{'action'} eq "do_search_dict" )	{ &do_search_dict; }
elsif	( $in{'action'} eq "search_dict" )	{ &search_dict(\$cgi_admin,
							       \%in); }
elsif	( $in{'action'} eq "do_download_file" )	{ &do_download_file; }
elsif	( $in{'action'} eq "download_file" )	{ &download_file; }
elsif	( $in{'action'} eq "do_show_ulist" )	{ &do_show_ulist; }
elsif	( $in{'action'} eq "do_pend_tlist" )	{ &do_pend_tlist; }
elsif	( $in{'action'} eq "do_pend_mass" )	{ &do_pend_mass; }
elsif	( $in{'action'} eq "do_mass_resolve" )	{ &do_mass_resolve; }
elsif	( $in{'action'} eq "do_user_modify" )	{ &do_user_modify; }
elsif	( $in{'action'} eq "do_add_form" )	{ &do_add_form; }
elsif	( $in{'action'} eq "do_term_list" )	{ &do_term_list; }
elsif	( $in{'action'} eq "do_letter_list" )	{ &do_letter_list; }
elsif	( $in{'action'} eq "do_add_local" )	{ &do_add_local; }
elsif	( $in{'action'} eq "do_upload_file" )	{ &do_upload_file; }
elsif	( $in{'action'} eq "upload_file" )	{ &upload_file; }
else	{ &do_login; }

# If we're embedding into a site - include these site specific files
if ($embed)
{
    &display_file(0, $file_right);
    &display_file(0, $file_footer);
}

# Normal exit
exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Print the appropriate html header
sub do_html_header
{
    # Passing-in a non-ZERO disables the printing of the header
    my ($disable_print)	= @_;

    if (! $disable_print) { print $header; }

    if ($embed)
    {
	&display_file($title_admin, $file_header);
    }
}

##
# Attain username and password to initiate login
sub do_login
{
    my (
	$blk_top,
	$blk_bot
       );

    if ($embed)
    {
	$blk_top = qq|
<tr>
<td align=left valign=top width="98%">
<br>
|;
	%blk_bot = qq|
</td>
|;
    }
    else
    {
	$blk_top = qq|
$doctype
<html>
<head>
<title>$str_admin (Login)</title>
</head>
<body>
|;
	$blk_bot = qq|
</body>
</html>
|;
    }
	
    print qq|
$blk_top
<h1 align="center">$str_login <br> Login</h1>
<br>
<form action="$cgi_admin" method="POST">
  <input type="hidden" name="action" value="login">
  <div align="center">
  <br>
  Please identify yourself below to login.
  <br>&nbsp;
  <table cellpadding="0" cellspacing="0">
  <tr>
   <td span style="background-color:#e7e7e7">Email:</td>
   <td span style="background-color:#e7e7e7"> 
   <input type="text" name="email" value=""></td>
  </tr>
  <tr>
   <td span style="background-color:#e7e7e7">Password:</td>
   <td span style="background-color:#e7e7e7"> 
   <input type="password" name="password" value=""></td>
  </tr>
  <tr>
   <td span style="background-color:#e7e7e7">
    <input type="reset"  value="Reset">
    <input type="submit" value="Login">
   </td>
   <td span style="background-color:#e7e7e7; text-align:center">
    <a href="$cgi_admin?action=do_getpass">
    <font size="-2">Forgot your password</font></a>
   </td>
  </tr>
  </table>
  </div>
</form>
<!-- ASR has requested this be disabled for now
<font size="-1"><center><a href="$cgi_admin?action=do_apply">Create Account</a>
 -->
</center>
</font>
$blk_bot
|;

}

##
# Actual login and Cookie setting
sub login
{
    if ( $in{'email'} !~ /\S+/ )
    { 
	&do_html_header();
	&error("ERROR: Can't complete your request",
	       "You must enter your email");
    }

    if ( $in{'password'} !~ /\S+/ )
    {
	&do_html_header();
	&error("ERROR: Can't complete your request",
	       "You must enter your password");
    }

    my ($privilege)	= &authenticate();

    # Priviliages :
    #  0 - not allowed/not found
    #  1 - admin
    #  2 - normal user
    if ( $privilege == 0 )
    {
	&do_html_header();
	&error("ERROR: Can't allow login", "You either misspelled your
               email/password <br> or <br> you don't have permission to
	       enter here.");
    }

    # Manage user's accepted session (expires in 2hours)
    my $time_expire	= mygmtime(time+(7200));
    my $time_now	= mygmtime(time);

    # After the cookie is set, we're not returning here due to forced reload
    &setCookie(
	       $privilege,
	       $in{'email'},
  	       "dict_email=$in{'email'}; ",
  	       "expires=$time_expire; ",
  	       "path=/; ",
  	       "domain=$dot_domain"
  	      );
}

##
# Logout from admin & end session (back-back on browser should not work)
sub logout
{

    # Clear session cookie (snap the trap :-)
    &setCookie(
	       0,
	       0,
    	       "dict_email=; ",
    	       "expires=$comp_big_bang; ",
    	       "path=$cookie_path; ",
    	       "domain=$dot_domain"
    	      );

    &do_html_header(1);

    # bye-bye
    &display("<center><h3>Come back soon...</h3></center>");
}

##
# Show db's contents in terms of user list (ulist)
sub do_show_ulist
{

    my ($heading, 
	$body)		= &show_ulist($in{'my_priv'}, $in{'my_email'});

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Show pending terms list
sub do_search_local
{
    my $search_form	= &displaysearchform(0, \$cgi_admin, \%in);

    # Find out the number of terms we have at hand
    my $dbh		= &connect();

    # Show either,
    # - newly entered (un-inspected) terms only
    # - approved terms only
    my $sql	= qq|
	SELECT id
	FROM   $dict_terms
	WHERE  can_use = ? AND IF (?, inspector = '-', 1)
|;

    my $sth	= $dbh->prepare_cached($sql) or
	&error("ERROR: Can't execute search query", $dbh->errstr);

    # Query un-inspected terms
    $sth->execute(0, 1);
    my $rows_0	= $sth->rows;

    # Query approved terms
    $sth->execute(1, 0);
    my $rows_1	= $sth->rows;

    &disconnect($dbh);

    my $heading		= qq|<font size="+1"><u><b>Search INTERNAL/LOCAL
                             terms ($rows_0/$rows_1):</b></u></font>|;

    &admin_display($in{'my_priv'}, $heading, $search_form);
}

##
# Display the search form
sub do_search_dict
{
    # Get the number of terms within the DICT server to display
    $in{'dictd_num'}	= &dictd_term_num($in{'dictd_num'}, \$cgi_admin);

    my $search_form	= &displaysearchform(1, \$cgi_admin, \%in);

    my $heading		= qq|<font size="+1"><u>
                             <b>Search DICT server ($in{'dictd_num'}):</b>
                             </u></font>|;

    &admin_display($in{'my_priv'}, $heading, $search_form);
}

##
# Show Download Options
sub do_download_file
{
    my $my_email = $in{'my_email'};
    my $my_priv	 = $in{'my_priv'};

    $heading	 = qq|<font size="+1"><u><b>Term Download:</b></u></font>|;
    $body	 = qq|
<br>
<b>You have two download options:</b><br>
- <a href="$cgi_admin?action=download_file&my_email=$my_email&my_priv=$my_priv&file=new">
  New <i>non-downloaded</i> Approved Terms</a><br>
- <a href="$cgi_admin?action=download_file&my_email=$my_email&my_priv=$my_priv&file=all">
  All Approved Terms</a><br>
|;

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Do the actual download of the file specified
sub download_file
{

    my ($data_num,
	$data_out);

    my $download_name	= "QaMoose_$in{'file'}.po";

    # Print straight to the Browser for interpretation
    print "Content-Type:application/x-download; charset=utf-8\n";
    print "Content-Disposition:attachment; filename=$download_name\n\n"; 

    # Fetch data with all/new differenication passed-in
    ($data_num,
     $ref_data_out)	= &download_sql_data( ($in{'file'} =~ /all/i) );

    %data_out		= %{$ref_data_out};

    if ($data_num)
    {
	my $date_utc;
	chop ($date_utc = `/bin/date -u '+%Y-%m-%d %H:%M+0000 (UTC)'`);

	print qq|# Translation suggestions from QaMoose (v-$version)
#-*-
#
# This file is NOT to be checked-in.  Its an auto-generated
# file which QaMoose spits out via its web-form based on
# instructions from a privileged admin/inspector.
#
# What is required of you now is to incorporate this file
# in the golden list of files that reside on the DICT server.
# In other words, use your hands (cut-n-paste) and/or any
# scripts you like to accomplish the task of augmenting your
# golden dictionary files with the terms listed here.
#
# - Copyright (C) 2003 Free Software Foundation, Inc.
#
# To generate a simple equality list, run this command-line
#   perl -n -e 'if (/^msgid\s+"(.*)"/)  { print "$1 = "; } \
#               if (/^msgstr\s+"(.*)"/) { print "$1\\n"; }' \
#               this_file_name_here
#
# $homepage
#
#-*-
#
msgid ""
msgstr ""
"Project-Id-Version: QaMoose_Words\\n"
"POT-Creation-Date: $date_utc\\n"
"PO-Revision-Date : $date_utc\\n"
"Last-Translator: QaMoose <noSpam\@nowhere.org>\\n"
"Language-Team: Arabic <doc\@nowhere.org>\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: KBabel 1.0.1\\n"
"PO-Revision-Date: $date_utc\\n"

|;
	for (my $i=0; $i<$data_num; $i++)
	{
	    print "# $data_out{$i}{'id'} :: ";
	    print "from, $data_out{$i}{'from_email'} :: ";
	    print "approved by, $data_out{$i}{'inspector'}\n";

	    if ($data_out{$i}{'description'})
	    {
		print "# Description - $data_out{$i}{'description'}\n";
	    }

	    print "msgid  \"$data_out{$i}{'english'}\"\n";
	    print "msgstr \"$data_out{$i}{'arabic'}\"\n\n";
	}
    }
    else
    {
	print "Your query yielded NO results - sorry \n";
    }
}

##
# Do the actual local MySQL accesses for the download data
sub download_sql_data
{
    my ($get_all)	= @_;

    my ($sql_misc,
	$read_out);

    my $dbh	= &connect();

    if (!$get_all) { $sql_misc	= "AND downloaded = 0"; }

    my $sql	= qq|
	SELECT   id, english, arabic, email, description, inspector
	FROM     $dict_terms
	WHERE    can_use = 1 AND hide = 0 $sql_misc
|;

    my $sth	= $dbh->prepare_cached($sql) or
	          &err("Can't execute search query", $DBI::errstr);

    $sth->execute;
    my $rows	= $sth->rows;

    # If there are no results, revert back
    if (!$rows)
    {
	# Clean-up the MySQL query structure
	$sth->finish;
	&disconnect($dbh);
	return(0, "NONE");
    }

    for (my $i=0; $i<$rows; $i++)
    {
	my $num	= $i + 1;

	my ($id,
	    $english,
	    $arabic,
	    $from_email,
	    $description,
	    $inspector)	= $sth->fetchrow_array();

	$read_out{$i}	= {
	                   "id"          => $id,
			   "english"     => $english,
			   "arabic"      => $arabic,
			   "from_email"  => $from_email,
			   "description" => $description,
			   "inspector"   => $inspector
			  };
    }

    # If I'm opting to get only the NEW terms; after getting them, set
    # the "downloaded" bit
    if (!$get_all)
    {
  	my $sql	= qq|
  	    UPDATE $dict_terms
  	    SET    downloaded	= '1'
  	    WHERE  can_use = 1 AND hide = 0 $sql_misc
|;

	my $sth	= $dbh->prepare_cached($sql) or
	          &err("Can't execute search query", $DBI::errstr);

	$sth->execute;
    }

    $sth->finish;
    &disconnect($dbh);
    return ($rows, \%read_out);
}

##
# Show pending terms list
sub do_pend_tlist
{
    my ($heading, 
	$body)		= &pend_tlist($in{'my_priv'}, $in{'my_email'});

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Show pending terms list in mass action form
sub do_pend_mass
{
    my ($heading, 
	$body)		= &pend_mass($in{'my_priv'}, $in{'my_email'});

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Search the local MySQL database for specific terms
sub search_local
{
    if ( $in{'query'} eq "" )
    { &err("ERROR: You must enter a search term"); }

    if ( ( $in{'A'} eq "" ) && ( $in{'E'} eq "" ) && ( $in{'D'} eq "" ) )
    { $in{'E'} = 1 }

    # Local variables to this function
    my (
	$show_all,
	$q,
	$sql,
       );

    my $dbh	= &connect();

    # Get rid of any preceding/trailing spaces
    $in{'query'} =~ s/^\s*(.*\S)\s*$/$1/;

  QUERY_CASE:
    {
        if ( $in{'query'} eq "*" )
        {
            $show_all		= 1;
            last QUERY_CASE;
        }

        if ( $in{'type'} eq "any" )
        {
            $in{'query'}        = '%' . "$in{'query'}" . '%';
            $q                  = "LIKE";
            last QUERY_CASE;
        }

        # default
        $q	= "=";
    }

    if ( $show_all )
    {
        $sql	= qq|
            SELECT english, arabic, latin, description, can_use
            FROM   $dict_terms
            WHERE  can_use = 1
|;
    }
    else
    {
	my (
	    $or_1,
	    $or_2,
	    $comp_e,
	    $comp_a,
	    $comp_d,
 	   );
	    
	my $query	= $dbh->quote($in{'query'});

	# Search the English, Arabic, & Description fields appropriately
	if ($in{'E'} && ($in{'A'} || $in{'D'})) { $or_1 = "OR"; }
	if ($in{'A'} &&  $in{'D'})              { $or_2 = "OR"; }

	if ( $in{'E'} eq "1" ) { $comp_e ="english     $q $query"; }
	if ( $in{'A'} eq "1" ) { $comp_a ="arabic      $q $query"; }
	if ( $in{'D'} eq "1" ) { $comp_d ="description $q $query"; }

	$sql	= qq|
	    SELECT english, arabic, latin, description, can_use
	    FROM   $dict_terms
	    WHERE  ($comp_e $or_1 $comp_a $or_2 $comp_d) AND can_use = 1
|;
    }

    my $sth	= $dbh->prepare_cached($sql) or
	          &err("ERROR: Can't execute search query", $dbh->errstr);
    $sth->execute;
    my $rows	= $sth->rows;
    my $content	= "";

    if (!$rows)
    {
 	$content = "<hr>" . &displaysearchform(0, \$cgi_admin, \%in);
  	&err("Sorry, the term you're seeking was NOT found
              <br>-or-<br> the term might have NOT been
              approved yet", $content);
    }

    # Determine the width of the spaces to include
    my $len	= (length($rows) * 4);
    my $spaces	= "&nbsp;" x $len;

    $content	.= qq|
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<b>Found $rows result(s):</b>
<tr><td>$spaces</td></tr>
|;

    for (my $i=0; $i<$rows; $i++)
    {
	my $num	= $i + 1;
	my ($english,
	    $arabic,
	    $latin,
	    $description)	= $sth->fetchrow_array();

	if ( $latin !~ /\S+/ ) { $latin = "[ n/a ]"; }

	$content	.= qq|
<tr>
  <td align="right">&nbsp; $num. &nbsp;</td>
  <td align="left"><font color="Red">English : $english</font></td>
</tr>
<tr>
  <td></td>
  <td align="left"><font color="Green">Arabic : $arabic</font></td>
</tr>
<tr>
  <td></td>
  <td align="left"><font color="Black">Latin : $latin</font></td>
</tr>
<tr>
  <td></td>
  <td align="left><font color="Blue">Description : $description</font></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
|;
    }

    $content	.= "</table>&nbsp;<br><hr>";
    $sth->finish;
    $content	.= &displaysearchform(0, \$cgi_admin, \%in);
    &disconnect($dbh);

    $heading	= qq|<font size="+1"><u><b>Search Form:</b></u></font>|;

    &admin_display($in{'my_priv'}, $heading, $content);

}

##
# Nitty Gritty of the pending terms list (tlist)
sub pend_tlist
{
    my ($my_priv,
	$my_email)	= @_;

    my $dbh		= &connect();

#		LIMIT 0, 10 DESC
    # List pending submissions in ascending order (time-list, oldest first)
    my $sql		= qq|
	SELECT   id, english, arabic, latin, email, description
	FROM     $dict_terms
	WHERE    can_use = 0 AND inspector = '-'
	ORDER BY id ASC
|;
    my $sth		= $dbh->prepare_cached($sql) or
			  &err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $pend_rows	= $sth->rows;

    my $content		= qq|
<table width="100%" border="0">
<tr>
<td span style="text-align:right">
  <a href="$cgi_admin?action=do_pend_tlist&my_email=$my_email&my_priv=$my_priv">[SingleResolve]</a>
  <a href="$cgi_admin?action=do_pend_mass&my_email=$my_email&my_priv=$my_priv">[MassResolve]</a>
</td>
</tr>
</table>
|;

    for (my $i = 0; $i < $pend_rows; $i++)
    {
	my ($id,
	    $english,
	    $arabic,
	    $latin,
	    $from_email,
	    $description)	= $sth->fetchrow_array();

	$content .= qq|
<br>
<b>English:</b><font color="red"> $english</font><br>
<b>Arabic:</b><font color="green"> $arabic</font><br>
<i><b>Latin:</b><font color="blue"> $latin</font></i><br>
<b>Description:</b> $description<br><br>
<b>Submitted by:</b> $from_email<br>
<table width="100%" border="0">
<tr>
<form action="$cgi_admin" method="POST">
<td><input type="radio"  name="decision" value="term_approve" checked>&nbsp;Approve</td>
<td><input type="radio"  name="decision" value="term_edit">&nbsp;Edit</td>
<td><input type="radio"  name="decision" value="term_reject">&nbsp;Reject</td>
<td><input type="hidden" name="action"   value="do_term_resolve">
<td><input type="hidden" name="my_email" value="$my_email">
<td><input type="hidden" name="my_priv"  value="$my_priv">
<td><input type="hidden" name="id"       value="$id">
<td><input type="hidden" name="source"   value="pend">
<input type="submit" value="Submit"></td>
</form>
</tr>
</table>
<br>
<hr>
|;
    }

    $sth->finish;

    &disconnect($dbh);

    if (!$pend_rows) { $content = ""; }

    $heading	= qq|<font size="+1"><u><b>Found &nbsp; $pend_rows &nbsp;
                     TERM(s) requiring approval:</b></u></font>|;

    return ($heading, $content);
}

##
# Pending terms (tlist) mass action (single button approve/delete for all)
sub pend_mass
{
    my ($my_priv,
	$my_email)	= @_;

    my $dbh		= &connect();

#		LIMIT 0, 10 DESC
    # List pending submissions in ascending order (time-list, oldest first)
    my $sql		= qq|
	SELECT   id, english, arabic, latin, email, description
	FROM     $dict_terms
	WHERE    can_use = 0 AND inspector = '-'
	ORDER BY id ASC
|;
    my $sth		= $dbh->prepare_cached($sql) or
			  &err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $pend_rows	= $sth->rows;

    my $content		= qq|
<table width="100%" border="0">
<tr>
<td span style="text-align:right">
  <a href="$cgi_admin?action=do_pend_tlist&my_email=$my_email&my_priv=$my_priv">[SingleResolve]</a>
  <a href="$cgi_admin?action=do_pend_mass&my_email=$my_email&my_priv=$my_priv">[MassResolve]</a>
</td>
</tr>
</table>

<form action="$cgi_admin" method="POST">
<input type="hidden" name="action"   value="do_mass_resolve">
<input type="hidden" name="my_email" value="$my_email">
<input type="hidden" name="my_priv"  value="$my_priv">
|;

    for (my $i = 0; $i < $pend_rows; $i++)
    {
	my ($id,
	    $english,
	    $arabic,
	    $latin,
	    $from_email,
	    $description)	= $sth->fetchrow_array();

	$content .= qq|
<br>
<b>English:</b><font color="red"> $english</font><br>
<b>Arabic:</b><font color="green"> $arabic</font><br>
<i><b>Latin:</b><font color="blue"> $latin</font></i><br>
<b>Description:</b> $description<br><br>
<b>Submitted by:</b> $from_email<br>
<table width="100%" border="0">
<tr>
<td><input type="radio"  name="decision_$i" value="term_approve" checked>&nbsp;Approve</td>
<td><input type="radio"  name="decision_$i" value="term_defer">&nbsp;Defer</td>
<td><input type="radio"  name="decision_$i" value="term_reject">&nbsp;Reject</td>
<td><input type="hidden" name="id_$i" value="$id"></td>
</tr>
</table>
<br>
<hr>
|;
    }

    # If there are any terms, null'ify content
    if ($pend_rows)
    {
	$content	.= qq|
<input type="hidden" name="num_of_pend" value="$pend_rows">
<br>
<center>
<input type="submit" value="Submit"></td>
</right>
</form>
</center>
|;
    }
    else
    {
	$content	= "";
    }

    $sth->finish;

    &disconnect($dbh);

    $heading	= qq|<font size="+1"><u><b>Found &nbsp; $pend_rows &nbsp;
                     TERM(s) requiring approval:</b></u></font>|;

    return ($heading, $content);
}

##
# Nitty Gritty of the mass term list action function
sub do_mass_resolve
{
    my (
	$can_use,
	$status,
	$inspector,
	$id,
       );

    my $dbh	= &connect();

    for (my $i = 0; $i < $in{'num_of_pend'}; $i++)
    {
	if ( $in{qq|decision_$i|} eq "term_approve" )
	{
	    $can_use	= 1;
	    $status	= "approved";
	    $inspector	= $in{'my_email'};
	}
	elsif ( $in{qq|decision_$i|} eq "term_reject" )
	{
	    $can_use	= 0;
	    $status	= "rejected";
	    $inspector	= $in{'my_email'};
	}
	elsif ( $in{qq|decision_$i|} eq "term_defer" )
	{
	    # Skip to next loop interation
	    next;
	}

  	$can_use	= $dbh->quote($can_use);
  	$status	        = $dbh->quote($status);
  	$inspector	= $dbh->quote($inspector);
  	$id		= $dbh->quote($in{qq|id_$i|});

  	my $sql	= qq|
  	    UPDATE $dict_terms
  	    SET    can_use		= $can_use,
  	           status		= $status,
  	           inspector	        = $inspector
  	    WHERE  id	= $id
|;
  	my $sth	= $dbh->prepare_cached($sql) or
  	    &err("Can't execute search query", $DBI::errstr);
  	$sth->execute;
  	$sth->finish;
      }

      &disconnect($dbh);

      my $heading;
      my $body;
      ($heading,
       $body)	= &pend_tlist($in{'my_priv'}, $in{'my_email'});

      &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Guts of term resolution
sub do_term_resolve
{
    my (
	$set,
 	$can_use,
	$status,
	$inspector,
	$edit,
	$id,
       );

    my $dbh	= &connect();

  CASE_TERM:
    {
	if ( $in{'decision'} eq "term_approve" )
	{
	    $set	= "SET";
	    $can_use	= 1;
	    $status	= "approved";
	    $inspector	= $in{'my_email'};
	    last CASE_TERM;
	}
	if ( $in{'decision'} eq "term_reject" )
	{
	    $set	= "SET";
	    $can_use	= 0;
	    $status	= "rejected";
	    $inspector	= $in{'my_email'};
	    last CASE_TERM;
	}
	if ( $in{'decision'} eq "term_edit" )
	{
	    &do_term_edit();
	    return;
	}
	if ( $in{'decision'} eq "term_submit" )
	{
  	    ($english,
  	     $arabic,
  	     $latin,
  	     $email,
  	     $description)	= &process_term_input(\%in);

  	    $english		= $dbh->quote($english);
  	    $arabic		= $dbh->quote($arabic);
  	    $latin		= $dbh->quote($latin);
  	    $description	= $dbh->quote($description);

	    # Super user can modify english terms
	    my $set_english;
	    if ($in{'my_priv'} == 1)
            {
		$set_english	= "english = $english,";
	    }

	    $edit		= qq|
		SET    $set_english
		       arabic		= $arabic,
		       latin		= $latin,
		       description	= $description,
|;
	    $can_use		= 1;
	    $status		= "editted";
	    $inspector		= $in{'my_email'};
	    last CASE_TERM;
	}
	&err("Can't do what you asked",
	     "You fell into a hole - CASE_TERM");
    }

    $can_use	= $dbh->quote($can_use);
    $status	= $dbh->quote($status);
    $inspector	= $dbh->quote($inspector);
    $id		= $dbh->quote($in{'id'});

    my $sql	= qq|
	UPDATE $dict_terms
	$edit
	$set   can_use		= $can_use,
	       status		= $status,
	       inspector	= $inspector
	WHERE  id	= $id
|;

    my $sth	= $dbh->prepare_cached($sql) or
		  &err("Can't execute search query", $DBI::errstr);

    $sth->execute;
    $sth->finish;
    &disconnect($dbh);

    my $heading;
    my $body;
    if ($in{'source'} eq "pend")
    {
	($heading,
	 $body)	= &pend_tlist($in{'my_priv'}, $in{'my_email'});
    }
    else
    {
	($heading,
	 $body)	= &letter_list();
    }

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Procedure to modify/edit a term
sub do_term_edit
{
    my $dbh	= &connect();

    my $id	= $dbh->quote($in{'id'});

    my $sql	= qq|
	SELECT id, english, arabic, latin, email, description
	FROM   $dict_terms
	WHERE  id = $id
|;
    my $sth	= $dbh->prepare_cached($sql) or
	          &err("Can't execute search query", $DBI::errstr);
    $sth->execute;

    my ($id,
	$english,
	$arabic,
	$latin,
	$from_email,
	$description)	= $sth->fetchrow_array();

    $sth->finish;
    &disconnect($dbh);

    # Super Admin can modify english terms as well
    my $head_english;
    my $line_english;
    if ($in{'my_priv'} == 1)
    {
	$line_english	= qq|
<td><input type="text" name="english" value="$english"></td>
|;
    }
    else
    {
	$head_english	= qq|
<input type="hidden" name="english" value="dont_care">
|;
	$line_english	= qq|
<td height="$height"><b>$english</b></td>
|;
    }	

    my $heading	= qq|<font size="+1"><u><b>Modify term - '$english'</b></u>
	             </font>|;
    my $body	= qq|
<center>
<form action="$cgi_admin" method="POST">
<input type="hidden" name="action"   value="do_term_resolve">
<input type="hidden" name="decision" value="term_submit">
<input type="hidden" name="id"       value="$in{'id'}">
$head_english
<input type="hidden" name="email"    value="dont_care">
<input type="hidden" name="my_email" value="$in{'my_email'}">
<input type="hidden" name="my_priv"  value="$in{'my_priv'}">
<table border>
<tr>
<td>English term: </td>
$line_english
</tr>
<tr>
<td>Arabic term: </td>
<td><input type="text" name="arabic" value="$arabic"></td>
</tr>
<tr>
<td>Arabic term in latin letters (optional): </td>
<td><input type="text" name="latin" value="$latin"></td>
</tr>
<tr>
<td height="$height">Submittor's email address:</td>
<td><b>$from_email</b></td>
</tr>
<tr>
<td colspan="2">
Description:<br><br>
<textarea name="description" cols="50" rows="5">$description
</textarea>
</td>
</tr>
<tr>
<td><input type="reset" value="Reset"></td>
<td><input type="submit" value="Approve"></td>
</tr>
</table>
</form>
|;
    
    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Same function name as in dict_main file - using same library call
sub do_add_local
{
    my $form		= &displayaddform(\$cgi_admin, \%in);

    ($english,
     $arabic,
     $latin,
     $email,
     $description)	= &process_term_input(\%in, \$form);

    my $dbh	 = &connect();
    $english	 =  $dbh->quote($english);
    $arabic	 =  $dbh->quote($arabic);
    $latin	 =  $dbh->quote($latin);
    $email	 =  $dbh->quote($email);
    $description =  $dbh->quote($description);

    # See if the term is already in the db
    my $sql	= qq|
	SELECT english
	FROM   $dict_terms
	WHERE  english = $english
|;
    my $sth	= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $dbh->errstr);

    $sth->execute;
    my $rows	= $sth->rows;

    $sth->finish;
    if ( $rows ne "0" )
    {
	&err("The term $english is already in the database",
	     "Make sure your term is unique");
    }

    # -------------------
    # Add to the database
    $sql	= qq|
	INSERT INTO $dict_terms
	       (id, english, arabic, latin, email, description, can_use,
                hide, status, inspector)
	VALUES ('', $english, $arabic, $latin, $email, $description, 1,
                0, 'inserted', $email)
|;

    $dbh->do($sql) or &err("Can't execute SQL query", $dbh->errstr);
    &disconnect($dbh);

    my $content	= qq|
<center>
<h3>Thank you\!</h3>
<br>
The following data has been added to the database.<br>
The term will be <b>available NOW</b>.
<br>&nbsp;
<table border>
<tr>
<td align="left">English: </td><td> $english</td>
</tr>
<tr>
<td align="left">Arabic: </td><td> $arabic</td>
</tr>
<tr align="left">
<td align="left">Arabic in Latin letters: </td><td> $latin</td>
</tr>
<tr>
<td align="left">Email: </td><td> $email</td>
</tr>
<tr>
<td align="left">Description: </td><td> $description</td>
</tr>
</table>
</center>
|;

#    $content	.= &displayaddform(\$cgi_admin, \%in);
    $content	.= "<br><hr>";

    $heading	= qq|<font size="+1"><u><b>TERM &nbsp; $english &nbsp;
                     Added:</b></u></font>|;

    &admin_display($in{'my_priv'}, $heading, $content);
}

##
# User applying for an account to approve/reject/edit terms
sub do_apply
{
    my (
	$hdr_info,
	$blk_top,
	$blk_bot
       );

    # See if this request came form a logged-in admin
    if ($in{'my_email'})
    {
	$hdr_info	= qq|
<center><br>You are in the process of adding a user who is
pre-approved<br>
<b>(ie. the approval procedure will be forgone)</b></center>
|;
    }
    else
    {
	$hdr_info	= qq|
<center><br>You are in the process of applying for access
to the dictionary in order to approve/reject/edit terms<br>
<b>(this application will be scrutinized)</b></center>
|;
    
	if ($embed)
	{
	    $blk_top	= qq|
   <tr>
   <td align=left valign=top width="98%">
   <br>
|;
	    $blk_bot	= qq|
    </td>
|;
	}
	else
	{
	    $blk_top	= qq|
$doctype
<html>
<head><title>Account Request</title></head>
<body>
|;
	    $blk_bot	= qq|
</body>
</html>
|;
	}
    }

    my $body	= qq|
$blk_top
$hdr_info
<form action="$cgi_admin" method="POST">
<input type="hidden" name="action" value="apply">
<input type="hidden" name="my_email" value="$in{'my_email'}">
<input type="hidden" name="my_priv"  value="$in{'my_priv'}">
<div align="center">
<table span style="background-color:#e7e7e7">
<tr>
  <td span style="background-color:#e7e7e7">Name:</td>
  <td span style="background-color:#e7e7e7">
    <input type="text" name="acct_name" value="$in{'acct_name'}">
  </td>
</tr>
<tr>
  <td span style="background-color:#e7e7e7">Email:</td>
  <td span style="background-color:#e7e7e7">
    <input type="text" name="acct_email" value="$in{'acct_email'}">
  </td>
</tr>
<tr>
  <td span style="background-color:#e7e7e7">Password:</td>
  <td span style="background-color:#e7e7e7">
    <input type="password" name="acct_pass1" value="">
  </td>
</tr>
<tr>
  <td span style="background-color:#e7e7e7">Reconfirm Password:</td>
  <td span style="background-color:#e7e7e7">
    <input type="password" name="acct_pass2" value="">
  </td>
</tr>
<tr>
  <td span style="background-color:#e7e7e7">
    <input type="reset" value="Reset">
    <input type="submit" value="Create Account">
  </td>
</tr>
</table>
</div>
</form>
$blk_bot
|;

    # See if this request came form a logged-in admin
    if ($in{'my_email'})
    {
	my $heading	= qq|<font size="+1"><u><b>Add Approved User:
	                     </b></u></font>|;
	&admin_display($in{'my_priv'}, $heading, $body);
    }
    else
    {
	print $body;
    }
}

##
# User applying for an account - actual guts
sub apply
{
    # Check on validity of the application
    if ( $in{'acct_name'} !~ /\S+/ )
    {
	if ( $in{'my_email'} )
	{ &err("Can't complete your request",
	       "You must enter the full-name"); }
	else
	{ &error("Can't complete your request",
		 "You must enter the full-name"); }
    }

    if ( $in{'acct_email'} !~ /\S+/ )
    {
	if ( $in{'my_email'} )
	{ &err("Can't complete your request",
	       "You must enter an email address"); }
	else
	{ &error("Can't complete your request",
		 "You must enter an email address"); }
    }

    # Ensure the minimum password length & validity
    my $pass_length      = length($in{'acct_pass1'});
    if ( $pass_length < $min_pass_len )
    {
	if ( $in{'my_email'} )
	{ &err("Can't complete your request",
	       "You must enter a valid password
	        ($min_pass_len characters minimum)"); }
	else
	{ &error("Can't complete your request",
		 "You must enter a valid password
	          ($min_pass_len characters minimum)"); }
    }

    if ( $in{'acct_pass1'} ne $in{'acct_pass2'} )
    {
	if ( $in{'my_email'} )
	{ &err("Can't complete your request",
	       "Make sure the passwords match"); }
	else
	{ &error("Can't complete your request",
		 "Make sure the passwords match"); }
    }

    my $crypt_pass	= crypt($in{'acct_pass1'}, $in{'acct_pass1'});

    my $add_priv;
    my $add_inspector;
    my $screen_data;
    if ($in{'my_email'})
    {
	# Admin adds normal users with his/her approval
	$add_priv	= "2";
	$add_inspector	= $in{'my_email'};
	$screen_data	= qq|
Please inform the proper account owner if its availability.
|;
    }
    else
    {
	$add_priv	= "";
	$add_inspector	= "-";
	$screen_data	= qq|
You will receive the administrator's approval decision via email.
|;
    }

    # Let's connect & query our database
    my $dbh		= &connect();
    my $acct_name	= $dbh->quote($in{'acct_name'});
    my $acct_email	= $dbh->quote($in{'acct_email'});
    my $acct_pass	= $dbh->quote($crypt_pass);
    my $acct_priv	= $dbh->quote($add_priv);
    my $acct_insp	= $dbh->quote($add_inspector);

    # See if the account is not already in the database
    my $sql	= qq|
	SELECT email
	FROM   $dict_users
	WHERE  email = $acct_email
|;

    my $sth	= $dbh->prepare_cached($sql) or
  		  &err("Can't execute search query", $DBI::errstr);

    $sth->execute;

    my $rows	= $sth->rows;

    $sth->finish;

    if ( $rows ne "0" )
    {
	&err("Can't complete your request",
	     "Account $acct_email already exists in the database.");
    }

    # Add account to db
    my $sql	= qq|
	INSERT INTO $dict_users
	            (id, name, email, password,
                     privilege, inspector, setting)
	VALUES      ('', $acct_name, $acct_email, $acct_pass,
                     $acct_priv, $acct_insp, '')
|;

    $dbh->do($sql) or &err("Can't execute SQL query", $DBI::errstr);
    &disconnect($dbh);

    my $text	= qq|
<center>
<h3">Thank you\!</h3>
<br>
Account $acct_email has been submitted.<br>
$screen_data
</center>
|;

    my $heading	= qq|<font size="+1"><u><b>User Added:</b></u></font>|;

    if ($in{'my_email'})
    {
	&admin_display($in{'my_priv'}, $heading, $text);
    }
    else
    {
	&display($text);

	# Notify super.admin of new user application
	my $subject  = "$fromsubject_u - $in{'acct_name'}";
	my $body     = qq|$myname just received a NEW USER application.

Applicant notes the following info,

 Name   - $in{'acct_name'}
 Email  - $in{'acct_email'}

Please resolve this applicant's status at your earliest convenience.

Regards,
 $fromname
|;

	&send_mail($admin_email, $subject, $body);
    }
}

##
# Procedure to request a New or change password
sub do_getpass 
{ 
    my (
	$request,
	$logged,
	$logged_2,
	$string,
	$type,
	$name,
	$mod_str,
	$blk_top,
	$blk_bot,
       );

    if ( $in{'logged'} )
    {
	$request	= "Please enter your NEW password";
	$logged		= qq|
<tr>
  <td height="$height" span style="background-color:#e7e7e7">Email:</td>
  <td span style="background-color:#e7e7e7">$in{'email'}</td>
</tr>
|;
 	$logged_2	= qq|
<tr>
  <td span style="background-color:#e7e7e7">Reconfirm Password:</td>
  <td span style="background-color:#e7e7e7">
    <input type="password" name="password2" value="">
  </td>
</tr>
|;
	$string		= "Password";
	$type		= "password";
	$name		= "password1";
	$mod_str	= "Change";
    }
    else
    {
	$request	= "Please enter your email to request a NEW password";
	$logged		= "";
 	$logged_2       = "";
	$string		= "Email";
	$type		= "text";
	$name		= "email";
	$mod_str	= "Send";

	if ($embed)
	{
	    $blk_top	= qq|
   <tr>
   <td align=left valign=top width="98%">
   <br>
|;
	    $blk_bot	= qq|
    </td>
|;
	}
	else
	{
	    $blk_top	= qq|
$doctype
<html>
<head><title>Request a New Password</title></head>
<body>
|;
	    $blk_bot	= qq|
</body>
</html>
|;
	}
    }

    my $body	= qq|
$blk_top
<center>
<br>
<h5>$request</h5>
<br>
</center>
<form action="$cgi_admin" method="POST">
<input type="hidden" name="action"  value="getpass">
<input type="hidden" name="email"   value="$in{'email'}">
<input type="hidden" name="my_priv" value="$in{'my_priv'}">
<input type="hidden" name="logged"  value="$in{'logged'}">
<div align="center">
<table span style="background-color:#e7e7e7">
$logged
<tr>
  <td span style="background-color:#e7e7e7">$string:</td>
  <td span style="background-color:#e7e7e7">
    <input type="$type" name="$name" value="">
  </td>
</tr>
$logged_2
<tr>
<td span style="background-color:#e7e7e7">
  <input type="reset" value="Reset">
  <input type="submit" value="$mod_str password">
</td>
</tr>
</table>
</div>
</form>
$blk_bot
|;
    if ( $in{'logged'} )
    {
	my $heading	= qq|<font size="+1"><u><b>Change Password:</b></u>
	                     </font>|;
	&admin_display($in{'my_priv'}, $heading, $body);
    }
    else
    {
	print $body;
    }
}

##
# Procedure to actual change/send one's password
sub getpass
{
    if ( $in{'email'} !~ /\S+/ ) 
    { 
	&err("Can't complete your request",
	     "You must enter your email"); 
    }

    if ( $in{'logged'} && ($in{'password1'} ne $in{'password2'}) )
    { 
	&err("Can't complete your request",
	     "Make sure the passwords match");
    }

    my $dbh	= &connect();
    my $email	= $dbh->quote($in{'email'});

    # -----------------------------------
    # Verify that the user already exists

    my $sql	= qq|
	SELECT *
	FROM   $dict_users
	WHERE  email = $email
|;
    my $sth	= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $rows	= $sth->rows;
    $sth->finish;

    if (!$rows)
    { 
	&err("Can't complete your request",
	     "That user/email doesn't exist in the database");
    }

    # If user is logged into admin - he'll supply password
    my $pass;
    if ( $in{'logged'} )
    {
	$pass	= $in{'password1'};
    }
    else
    {
	# ---------------------------------
	# Get a new password and encript it
	#     $pass       <-- Generated password
	#     $pass_c     <-- Encrypted password
	#     $pass_cq    <-- Encrypted "quoted" password
	my @passet	= ('a'..'k', 'm'..'z', 'A'..'N', 'P'..'Z', '2'..'9');
	my $a		= time();
	srand($a);
	for (my $i = 0; $i < 8; $i++)
	{
	    my $random_num	= int(rand($#passet + 1));
	    $pass	       .= $passet[$random_num];
	}
    }

    my $pass_c	= crypt($pass, $pass);
    my $pass_cq	= $dbh->quote($pass_c);

    # -----------------------------------
    # Update record in the database
    $sql	= qq|
	UPDATE $dict_users
	SET    password = $pass_cq
	WHERE  email = $email
|;
    $dbh->do($sql) or &err("Can't execute SQL query", $DBI::errstr);
    &disconnect($dbh);

    if ( $in{'logged'} )
    {
	my $heading	= qq|<font size="+1"><u><b>Change Password:</b></u>
	                     </font>|;
	&admin_display(
		       $in{'my_priv'}, $heading,
		       "<center>OK, your password has been modified.</center>"
		      );
    }
    else
    {
	# Send the email
	my $subject	= $fromsubject_p;
	my $body	= qq|
/u{$domain} recently received a requested for a new password
for your dictionary account.  We are simply complying 8\)

 Your new random password is: $pass

Make sure to change it upon your next login to the dictionary
administartive page (admin).

Thanks and have fun...

Regards,
 $fromname
|;

	&send_mail($in{'email'}, $subject, $body);

        &display("<center><h3>Mail sent with your new password...</h3>
                  </center>");
    }
}

##
# Procedure to show email to be sent when applicants are "rejected"
sub show_mail
{
    my $to	= shift;
    my $subject	= shift;
    my $body	= shift;
    
    my $html_from = "<$fromemail>";
    my $form	= qq|
<form action="$cgi_admin" method="POST">
<input type="hidden" name="action"   value="do_user_resolve">
<input type="hidden" name="decision" value="$in{'decision'}">
<input type="hidden" name="email"    value="$in{'email'}">
<input type="hidden" name="my_email" value="$in{'my_email'}">
<input type="hidden" name="my_priv"  value="$in{'my_priv'}">
<input type="hidden" name="to"       value="$to">
<input type="hidden" name="subject"  value="$subject">
<table border="0">
<tr>
 <td>To: </td>
 <td>$to</td>
</tr>
<tr>
 <td>From: </td>
 <td>$fromname &lt;$fromemail&gt;</td>
</tr>
<tr>
 <td>Subject: </td>
 <td>$subject</td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
 <td>Mail Message:</td>
</tr>
<tr>
 <td colspan="2">
 &nbsp;&nbsp;<textarea name="body" cols="60" rows="15">$body</textarea>
</tr>
<tr>
 <td>
  <input type="reset"  value="Reset">
  <input type="submit" value="Send">
 </td>
</tr>
</table>
</form>
|;

    my $heading	= qq|<font size="+1"><u><b>Confirm Mail:</b></u>
	             </font>|;
    &admin_display($in{'my_priv'}, $heading, $form);
}

##
# Procedure to send email to term inspectors (applicants)
sub send_mail
{
    my $to	= shift;
    my $subject	= shift;
    my $body	= shift;

    # Some security clean-up (one can't be paranoid enough - untaint)
    $to		=~ s/(\;|\>|\<|\||\\n\f\r)//gi;

    # Don't mail crap to the 'net while debugging
    if ( $debug )
    { 
   print qq|
<h2>MAIL - mail to be sent:</h2><br>
To: $to <br>
From: $fromname <$fromemail><br>
Subject: $subject<br><br>
<pre>
$body<br>
</pre>
|;
    }
    else
    {
	# Send the specified email
	open (MAIL, "| $sendmail -t -oi") or
	     &err("Can't open sendmail", $!);
	print MAIL "To: $to\n";
	print MAIL "From: $fromname <$fromemail>\n";
	print MAIL "Subject: $subject\n\n";
	print MAIL $body;
	close(MAIL);
    }
}

##
# Procedure to display admin page
sub admin_display
{
    my ($my_priv,
	$heading,
	$content)	= @_;

    my $my_email	= $in{'my_email'} || $in{'email'};
    my $super_user;
    my $menu;

    my %cookie		= &getCookie();

    if ( (!defined $cookie{'dict_email'}) || 
	 ($cookie{'dict_email'} ne $my_email) )
    {
	if ( $debug )
	{ print "<br>DBG (COOKIE) - NO Cookie"; }
	# Kick-out intruder
	&error("ERROR: Can't continue your session",
	       "Ensure your cookies are enabled and Re-login");
    }
    else
    {
	if ( $debug )
	{ print "<br>DBG (COOKIE) - got my cookie :-)"; }
    }

    # Prepare the links menu
    if ( $my_priv eq "1" )
    {
	$super_user	= qq|
<b>Users:</b><br>
- <a href="$cgi_admin?action=do_pend_ulist&my_email=$my_email&my_priv=$my_priv">Pending</a><br>
- <a href="$cgi_admin?action=do_apply&my_email=$my_email&my_priv=$my_priv">Add</a><br>
- <a href="$cgi_admin?action=do_show_ulist&my_email=$my_email&my_priv=$my_priv">Modify</a><br>
<br>
|;
    }

    $menu	= qq|
<b>Terms:</b><br>
- <a href="$cgi_admin?action=do_pend_tlist&my_email=$my_email&my_priv=$my_priv">Pending</a><br>
- <a href="$cgi_admin?action=do_add_form&my_email=$my_email&my_priv=$my_priv">Add</a><br>
- <a href="$cgi_admin?action=do_letter_list&my_email=$my_email&my_priv=$my_priv">Modify</a><br>
- <a href="$cgi_admin?action=do_upload_file&my_email=$my_email&my_priv=$my_priv">Upload</a><br>
- <a href="$cgi_admin?action=do_download_file&my_email=$my_email&my_priv=$my_priv">Download</a><br>
- <a href="$cgi_admin?action=do_search_local&my_email=$my_email&my_priv=$my_priv">Search</a><br>
- <a href="$cgi_admin?action=do_search_dict&my_email=$my_email&my_priv=$my_priv">DICT.Search</a><br>
<br>
$super_user
<b>Account:</b><br>
- <a href="$cgi_admin?action=do_getpass&email=$my_email&my_priv=$my_priv&logged=1">Password</a><br>
- <a href="$cgi_admin?action=logout">Logout</a><br>
<br>
<br>
|;

    my (
	$blk_top,
	$blk_bot
       );

    if ($embed)
    {
	$blk_top = qq|
   <tr>
   <td align=left valign=top width="98%">
   <br>
|;
	$blk_bot = qq|
    </td>
|;
    }
    else
    {
	$blk_top = qq|
$doctype
<html><head><title>$str_admin (Panel)</title></head><body>
|;
	$blk_bot = qq|
</body></html>
|;
    }
    # Print the final page
    print qq|
$blk_top
<table width="100%" border="0">
  <td span style="color:#006600; background-color:#e7e7e7;
                  vertical-align:middle" colspan="2">
    <b>Welcome - <i>$my_email</i></b>
  </td>
  <td span style="text-align:center; background-color:#e7e7e7">
    <a href="$homepage" style="text-decoration:none">$myname<br>[$version]</a>
  </td>
</tr>
<tr>
  <td span style="background-color:#e7e7e7" width="16%" valign="top">
$menu
  </td>
  <td valign="top" width="100%">
$heading
  <br>
$content
  </td>
</tr>
</table>
$blk_bot
|;

}

##
# Form to query user of upload information
sub do_upload_file
{
    my $heading	= qq|<font size="+1"><u><b>Upload file:</b></u></font>|;

    my $format  = qq|
<h3>New Term format:</h3>
<table width="100%" border="0">
<tr>
  <td>&nbsp;</td>
  <td><b>new_term</b></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;<b>{</d></td>
</tr>
<tr><td>&nbsp;</td>
<td>&nbsp;&nbsp;<b>
    english</b></td><td> <b>= (</b>Your_english_term_here<b>)</b>
  </td>
</tr>
<tr><td>&nbsp;</td>
<td>&nbsp;&nbsp;<b>
    arabic</b></td><td>  <b>= (</b>Your_arabic_term_here<b>)</b>
  </td>
</tr>
<tr><td>&nbsp;</td>
<td>&nbsp;&nbsp;<b>
    latin</b></td><td>   <b>= (</b>Your_arabic_term_in_latin_letter<b>)</b>
  </td>
</tr>
<tr><td>&nbsp;</td>
<td>&nbsp;&nbsp;<b>
    description</b></td><td> <b>= (</b>A one-line or multi-line entry<b>)</b>
  </td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;<b>}</b></td>
</tr>
</table>
<br>
<center>All that is in <b>BOLD</b> is mandatory syntax/keywords.</center>
|;

    my $body	= qq|
$format
<center>
<form action="$cgi_admin" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action"    value="upload_file">
<input type="hidden" name="my_email"  value="$in{'my_email'}">
<input type="hidden" name="my_priv"   value="$in{'my_priv'}">
<input type="file"   name="upload_fn" size="40%">
<table width="100%" border="0">
<tr>
  <td align="center">
    <input type="reset"  value="Reset">
    <input type="submit" value="Upload File..">
  </td>
</tr>
</table>
</div>
</form>
|;

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Parse uploaded term file (done twice, upon upload and upon commit)
sub upload_file
{
    my $num		= 0;
    my $inside_term	= 0;
    my $prev_new_term	= 0;
    my $done		= 0;
    my %assoc_list;
    my $is_defined;

    my $up_filename	= $uploaded_file || $in{'uploaded_file'};

    my @word_order	= qw(english arabic latin description);

    my %words		= (
		           "english"		=> 0,
			   "arabic"		=> 1,
			   "latin"		=> 2,
			   "description"	=> 3
    			  );

    open(UP_IN, "< $up_filename") or 
	&err("Can't continue with upload", "Can't open $up_filename");

    while (<UP_IN>)
    {
	my $ln	= $num + 1;
	my %assoc_checker;
	if ( /new_term/ || $prev_new_term || $inside_term )
	{
	    if ( /\{/ )
	    {
		if ( $done )
		{
		    &err("Upload file Parse problem.",
			 "Your Entry #$ln has a format error.
			  Line, $INPUT_LINE_NUMBER.");
		}
		$inside_term	= 1;
		next;
	    }

	    if ( /new_term/ )
	    {
		$prev_new_term	= 1;
		next;
	    }
	    else
	    {
		$prev_new_term	= 0;
	    }

	    if ( /\}/ )
	    {
		# If New term is complete - check we got all we need
		if ( $done != 0xF )
		{
		    &err("Upload file Parse problem.",
			 "Your Entry #$ln has a term missing.
			  Line, $INPUT_LINE_NUMBER.");
		}

		# Get ready for next term
		$inside_term	= 0;
		$done		= 0;
		$num++;
		next;
	    }

	    # Check for each of the terms
	    foreach $term (keys %words)
	    {
		# If the term is defined - user's file has errors
		if ( defined $assoc_list{$num}{$term} && /$term\s*=/ )
		{
		    &err("Upload file Parse problem.",
			 "Your Entry #$ln is a duplicate.
			  Line, $INPUT_LINE_NUMBER.");
		}

		# See if term is matched -- "term = (string string"
		if ( /\s*$term\s*=\s*\((.*)/i )
		{
		    # Single line definition (have the ')' ?)
		    if ( /\s*$term\s*=\s*\((.*)\)/i )
		    {
			my ($ref_list,
			    $ref_check)	= &upload_tcheck(
							 $num,
							 $term,
							 $1,
							 \%assoc_list,
							 \%assoc_check
						        );
			%assoc_list	= %{$ref_list};
			%assoc_check	= %{$ref_check};
		    }
		    else
		    {
			# Multi-line definition
			my $out	= $1;
			while (<UP_IN>)
			{
			    # Find the line containing the ')'
			    if ( /^\s*(.*)\)/ )
			    {
				$out		.= $1;
				my ($ref_list,
				    $ref_check)	= &upload_tcheck(
								$num,
								$term,
								$out,
								\%assoc_list,
								\%assoc_check
							       );
				%assoc_list	= %{$ref_list};
				%assoc_check	= %{$ref_check};
				last;
			    }
			    else
			    {
				# Remove initial line spaces
				s/^\s*//;
				s/\n/ /;

				# Tack-on multi-line entry
				$out .= $_;
			    }
			}
                    }
		}
		$is_defined	= ( defined $assoc_list{$num}{$term} );
		$done		= ($done | $is_defined << $words{$term});
	    }
	}
    }
    close(UP_IN);

    my $heading;
    my $comment;
    my $nxt_step;
    if ( $in{'uploaded_file'} )
    {
	# Delete uploaded file, we're done with it
	if (! $debug )
	{
	    # Untaint - just in case
	    $in{'uploaded_file'} =~ /(^\/.*)\/(.*)/;
	    unlink ("$upload_path/$2");
	}

	$heading	= qq|<font size="+1">
	                     <u><b>Upload Commit:</b></u></font>|;
	&upload_commit(0, $num, \%assoc_list);
	$comment	.= "<center>Database updated - <b>$num</b>
                            New terms integrated.<br><br>
                            <h2> Thank You !! </h2></center>";
    }
    else
    {
	$heading	= qq|<font size="+1">
                             <u><b>Upload Inspect:</b></u></font>|;

	my $err_out	= &upload_commit(1, $num, \%assoc_list);

	if ( defined $err_out )
	{
	    $nxt_step	= qq|
<hr width="80%" align=center>
<br>
<center>There were <font color="red">ERRORS </font>in your uploaded file.<br>
$err_out
<br><br>
Please correct problem(s) and upload again.
|;
	}
	else
	{
	    # Query uploader to double-check his/her terms
	    $nxt_step	= qq|
<center><br>
<font color="green">All Terms checked and are clean - verify content.</font>
<form action="$cgi_admin" method="POST">
<input type="hidden" name="action"        value="upload_file">
<input type="hidden" name="my_email"      value="$in{'my_email'}">
<input type="hidden" name="my_priv"       value="$in{'my_priv'}">
<input type="hidden" name="uploaded_file" value="$uploaded_file">
<br>
<input type="submit" value="Proceed - integrate into db..">
</form>
|;
	}

	$comment	.= qq|<center><table border="0" cellpadding="0"
		              cellspacing="0" width="100%">|;

	for($i=0; $i < $num; $i++)
	{
	    my $k	= $i + 1;
	    $comment	.= qq|<tr><td align="left"><b>&nbsp; $k.
		                      </b></td>|;

	    my $tag	= "";
	    foreach $term (@word_order)
	    {
		$comment	.= qq|$tag<td align="left">\u$term</td>
 		                      <td>: $assoc_list{$i}{$term}</td></tr>|;
		$tag	 = "<tr><td>&nbsp;</td>";
	    }
	    $comment	.= "<tr><td>&nbsp;</td></tr>";
	}
	$comment	.= qq|</table></center>|;
    }

    my $body	= qq|
$comment
$nxt_step
|;

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Verify new terms' validity and consume 'em
sub upload_tcheck
{
    my ($cur_num,
	$cur_term,
	$cur_new_def,
	$ref_assoc_list,
	$ref_assoc_checker)	= @_;

    my %cur_assoc_checker	= %{$ref_assoc_checker};
    my %cur_assoc_list		= %{$ref_assoc_list};
    my $cur_entry_num		= $cur_num + 1;

    # See that the terms wasn't defined in list given
    if ( $cur_term eq "english" )
    {
	if ( $cur_assoc_checker{$cur_new_def} )
	{
	    &err("Upload file Parse problem.",
		 "Your Entry #$cur_entry_num duplicate entry within file.
                  Line, $INPUT_LINE_NUMBER.");
	}
	$cur_assoc_checker{$cur_new_def} = 1;
    }
    
    # See that the terms wasn't defined in list given
    if ( $cur_term eq "arabic" )
    {
	if ( &bad_utf8($cur_new_def) )
	{
	    &err("Upload file Parse problem.",
		 "Your Entry #$cur_entry_num - Arabic must be UTF-8 encoded.
                  Line, $INPUT_LINE_NUMBER.");
	}
    }

    # See that the required definition are defined :-)
    if ( ($cur_new_def !~ /\S/) && ($cur_term ne "latin") )
    {
	&err("Upload file Parse problem.",
	     "Your Entry #$cur_entry_num is missing its definition.
              Line, $INPUT_LINE_NUMBER.");
    }

    $cur_assoc_list{$cur_num}{$cur_term} = $cur_new_def;

    return(\%cur_assoc_list, \%cur_assoc_checker);
}

##
# Probe database for new terms (check existance; do insertion)
sub upload_commit
{
    my ($check_only,
	$num_terms,
	$ref_assoc_term)	= @_;

    my %assoc_term	= %{$ref_assoc_term};

    my ($error_out);
    
    my $dbh		= &connect();

    # See if english term is already in db
    if ( $check_only )
    {
	for(my $i=0; $i < $num_terms; $i++)
	{
	    my $new_term	= $dbh->quote($assoc_term{$i}{'english'});

	    my $sql		= qq|
		SELECT id, english
		FROM   $dict_terms
		WHERE  english = $new_term
|;
	    my $sth		= $dbh->prepare_cached($sql) or
		&err("Can't execute search query", $DBI::errstr);
	    $sth->execute;

	    # If we have hits in the db - that's an error
	    if ( $sth->rows )
	    {
		my ($id,
		    $english)	= $sth->fetchrow_array();

		my $k = $i+1;
		$error_out	.= "<br>Entry $k where <b>english = '$english'</b>
                                    already exists.";
	    }
	}
	return ($error_out);
    }

    my $dbh		= &connect();

    for(my $i=0; $i < $num_terms; $i++)
    {
	# If we got to this point, then proceed with insertion :-)
	my $new_english		= $dbh->quote($assoc_term{$i}{'english'});
	my $new_arabic		= $dbh->quote($assoc_term{$i}{'arabic'});
	my $new_latin		= $dbh->quote($assoc_term{$i}{'latin'});
	my $new_description	= $dbh->quote($assoc_term{$i}{'description'});
	my $my_email		= $dbh->quote($in{'my_email'});

	my $sql	= qq|
	    INSERT INTO $dict_terms
		        (id, english, arabic, latin, email,
		         description, can_use, hide, status, inspector)
	    VALUES      ('', $new_english, $new_arabic, $new_latin, $my_email,
		         $new_description, 1, 0, 'uploaded', $my_email)
|;

	$dbh->do($sql) or &err("Can't execute SQL query", $dbh->errstr);
    }
    &disconnect($dbh);
}

##
# Procedure to invoke the creation of latin letter list
sub do_letter_list
{
    my ($heading,
	$body)		= &letter_list();

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# Procedure to list/link latin alphabet letters for db search
sub letter_list
{
    my $my_email	= $in{'my_email'};
    my $my_priv		= $in{'my_priv'};

    # Generate the alphabet list
    my @alph_list	= ('A'..'Z');

    my $heading		= qq|<center><font size="+1"><u><b>[ </b></u> |;
    foreach $letter (@alph_list)
    {
	$heading	.= qq|
<a href="$cgi_admin?action=do_term_list&letter=$letter&my_email=$my_email&my_priv=$my_priv">$letter </a>
|;
    }

    $heading	.= "<u><b> ]</b></u></font></center>";

    my $body	= qq|
<center>Please select a letter from the list above !!<br>
A DataBase dump of all the terms starting with that letter will follow.
</center>
|;

    return ($heading, $body);
}

##
# Procedure to list database terms
sub do_term_list
{

    my $my_email	= $in{'my_email'};
    my $my_priv		= $in{'my_priv'};

    my $first_letter	= "$in{'letter'}" . '%';

    my $dbh		= &connect();

    my $first_letter	= $dbh->quote($first_letter);

    # -----------------------------
    # Get the latest 10 submittions
#		LIMIT 0, 10
    my $sql	= qq|
	SELECT   id, english, arabic, latin, email, can_use, status,
                 inspector, description
	FROM     $dict_terms
	WHERE    english LIKE $first_letter
|;
    my $sth		= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $hit_rows	= $sth->rows;

    my $body;
    for (my $i = 0; $i < $hit_rows; $i++)
    {
	my ($id,
	    $english,
	    $arabic,
	    $latin,
	    $from_email,
	    $can_use,
	    $status,
	    $inspector,
	    $description)	= $sth->fetchrow_array();

	my $action_option;
	if ( $can_use )
	{
	    $can_use		= "YES";
	    $action_option	= qq|
<td><input type="radio"  name="decision" value="term_reject"
checked>&nbsp;REJECT</td>
|;
	}
	else
	{
	    $can_use		= "NO";
	    $action_option	= qq|
<td><input type="radio"  name="decision" value="term_approve"
checked>&nbsp;Approve</td>
|;
	}

	$inspector	=~ s/-/N\/A/;
	$status		= $status || "Not inspected";

	$body		.= qq|
<b>English:</b><font color="red"> $english</font><br>
<b>Arabic:</b><font color="green"> $arabic</font><br>
<i><b>Latin:</b><font color="blue"> $latin</font></i><br>
<b>Public/Can Use:</b> $can_use</font><br>
<b>Status:</b> $status</font><br>
<b>Inspector:</b> $inspector</font><br>
<b>Description:</b> $description<br><br>
<b>Submitted by:</b> $from_email<br>
<table width="100%" border="0">
<tr>
<form action="$cgi_admin" method="POST">
$action_option
<td><input type="radio"  name="decision" value="term_edit">&nbsp;Edit</td>
<td><input type="hidden" name="action"   value="do_term_resolve">
<td><input type="hidden" name="my_email" value="$my_email">
<td><input type="hidden" name="my_priv"  value="$my_priv">
<td><input type="hidden" name="id"       value="$id">
<td><input type="hidden" name="source"   value="letter">
<input type="submit" value="Submit"></td>
</form>
</tr>
</table>
<hr><br>
|;
    }

    $sth->finish;

    &disconnect($dbh);

    my ($heading,
	$let_body)	= &letter_list();

     $heading		.= qq|<br><font size="+1"><u><b>Found &nbsp;
                              $hit_rows &nbsp; TERM(s) for '$in{'letter'}'
                              within dB:</b></u></font>|;

    &admin_display($my_priv, $heading, $body);
}

##
# Procedure to add New pre-approved terms
sub do_add_form
{
    my $add_form	= &displayaddform(\$cgi_admin, \%in);

    my $heading		= qq|<font size="+1"><u><b>Add a NEW approved
                             term:</b></u></font>|;

    &admin_display($in{'my_priv'}, $heading, $add_form);
}

##
# Procedure to decide on fate of term inspector applicants
sub do_user_resolve
{
    my $priv_value;
    my $body;

    my $subject	= "Your dictionary application has been - $in{'decision'}";

    if ( $in{'decision'} eq "accepted" )
    {
	$priv_value  = 2;
	$body	     = qq|Welcome to ${domain}'s dictionary interface ($myname).

Please take extra care in which terms you accept/reject
due to the importance of your actions as well as their
subsequent grand ramifications.

Some common questions (FAQ):

 Q. What exactly fits under 'description' ?
 A. Short description in the english language,
    do not cut/paste from copy-righted material.

 Q. What term sources can be used ?
 A. Everything can be a source, but a variety of
    sources is very important and essential (ie. don't
    keep using the SAME source - variety is best).

When in doubt, ask...

Regards,
 $fromname v-$version
|;
    }
    else
    {
	$priv_value	= 0;
        $body           = $in{'body'} || qq|
We're sorry, but we've reached our intended limit
for dictionary inspectors.  As you might imagine, this
effort can not involve everyone since a cap must be
adhered to.

Please consider contributing terms since that does
not involve or require an account.

Regards,
 $fromname v-$version
|;
    }

    my $dbh		= &connect();

    my $email		= $dbh->quote($in{'email'});
    my $my_email	= $dbh->quote($in{'my_email'});

    # Check to see if NEW user is in db -- this is for the browser reload case
    my $sql	= qq|
	SELECT id
	FROM   $dict_users
	WHERE  email = $email AND inspector = '-'
|;
    my $sth		= $dbh->prepare_cached($sql) or
			  &err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $user_is_new	= $sth->rows;

    # Give inspector option to customize rejection e-mail
    if ( (!defined $in{'body'}) && ($in{'decision'} ne "accepted") )
    {
        &show_mail($in{'email'}, $subject, $body);
        return();
    }

    # Update user with decision irrespective of db contents
    my $sql	= qq|
	UPDATE $dict_users
	SET    privilege = $priv_value,
	       inspector = $my_email
	WHERE  email = $email
|;
    my $sth	= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $DBI::errstr);
    $sth->execute;

    $sth->finish;
    &disconnect($dbh);

    # Don't keep sending mail if inspector does reload in his/her browser
    if ( $user_is_new )
    {
	&send_mail($in{'email'}, $subject, $body);
    }
    else
    {
	if ( $debug )
	{ print "NO more mail"; }
    }

    # Re-display pending users
    &do_pend_ulist($in{'my_priv'}, $in{'my_email'});
}

##
# Show pending user-list; resolve user who'd applied (ie. accept or reject)
sub do_pend_ulist
{
    my ($my_priv,
	$my_email)	= @_;

    $my_priv		= $my_priv  || $in{'my_priv'};
    $my_email	        = $my_email || $in{'my_email'};

    my $dbh		= &connect();

    # Get pending users who had applied to join
    my $sql		= qq|
	SELECT id, name, email
	FROM   $dict_users
	WHERE  privilege = 0 AND inspector = '-'
|;
    my $sth		= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $pend_users	= $sth->rows;
    my $body;

    if (! $pend_users )
    {
	$body	= "";
    }
    else
    {
	for (my $i = 0; $i < $pend_users; $i++)
	{
	    my ($id,
		$name,
		$email)	 = $sth->fetchrow_array();

	    $body	.= qq|
<br>
<b>Name:</b> $name<br>
<b>Email:</b> $email<br>
<table width="100%" border="0">
<tr>
<form action="$cgi_admin" method="POST">
<td><input type="radio"  name="decision" value="accepted" checked>&nbsp;Approve</td>
<td><input type="radio"  name="decision" value="rejected">&nbsp;Reject</td>
<td><input type="hidden" name="action"   value="do_user_resolve">
    <input type="hidden" name="email"    value="$email">
    <input type="hidden" name="my_email" value="$my_email">
    <input type="hidden" name="my_priv"  value="$my_priv">
<input type="submit" value="Submit"></td>
</form>
</tr>
</table>
<br>
<hr>
|;
	}
    }

    $sth->finish;

    &disconnect($dbh);

    $heading	= qq|<font size="+1"><u><b>Found &nbsp; $pend_users &nbsp;
                     USER(s) seeking approval:</b></u></font>|;

    &admin_display($my_priv, $heading, $body);
}

##
# Modify status of a user - only super.admin can does this
sub do_user_modify
{
    my ($my_priv,
	$my_email)	= @_;

    my $privilege;
    my $inspector;
    my $id;

  CASE_USER:
    {
	if ( $in{'decision'} eq "user_delete" )
	{
	    $privilege	= 0;
	    $inspector	= $in{'my_email'};
	    last CASE_USER;
	}
	if ( $in{'decision'} eq "user_approve" )
	{
	    $privilege	= 2;
	    $inspector	= $in{'my_email'};
	    last CASE_USER;
	}
#	if ( $in{'decision'} eq "user_edit" )
#	{
#	    &do_term_edit();
#	    return;
#	}
	&err("Can't do what you asked",
	     "You fell into a hole - CASE_USER");
    }

    my $dbh	= &connect();

    $privilege	= $dbh->quote($privilege);
    $inspector	= $dbh->quote($inspector);
    $id		= $dbh->quote($in{'id'});

    my $sql	= qq|
	UPDATE $dict_users
	SET    privilege	= $privilege,
	       inspector	= $inspector
	WHERE  id	= $id
|;
    my $sth	= $dbh->prepare_cached($sql) or
		  &err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    $sth->finish;
    &disconnect($dbh);

    my ($heading,
        $body)	= &show_ulist($in{'my_priv'}, $in{'my_email'});

    &admin_display($in{'my_priv'}, $heading, $body);
}

##
# List/Show ALL current users in db
sub show_ulist
{
    my ($my_priv,
	$my_email)	= @_;

    $my_priv		= $my_priv  || $in{'my_priv'};
    $my_email		= $my_email || $in{'my_email'};

    my $dbh		= &connect();

    # Get a list of all the users
    my $sql		= qq|
	SELECT id, name, email, privilege, inspector 
	FROM   $dict_users
        WHERE  privilege >= 0
|;
    my $sth		= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $DBI::errstr);
    $sth->execute;
    my $num_users	= $sth->rows;

    my $body;
    if (! $num_users )
    {
	$body	= "";
    }
    else
    {
	for (my $i = 0; $i < $num_users; $i++)
	{
	    my ($id,
		$name,
		$email,
		$privilege,
		$inspector)	= $sth->fetchrow_array();

	    my $priv_txt;
	  CASE_PRIV:
	    {
		if ( $privilege == 0 )
		{
		    if ($inspector eq "-")
		    {
			$priv_txt	= "Not Inspected";
		    }
		    else
		    {
			$priv_txt	= "Rejected/Unapproved !!";
		    }
		    last CASE_PRIV;
		}
		if ( $privilege == 1 )
		{
		    $priv_txt		= "admin - SUPER";
		    last CASE_PRIV;
		}
		if ( $privilege == 2 )
		{
		    $priv_txt		= "admin - NORMAL";
		    last CASE_PRIV;
		}
		&err("Can't do what you asked",
		     "You fell into a hole - CASE_PRIV");
	    };

	    my $button_line;
	    if ( $email eq $in{'my_email'} )
	    {
		# No self-mutilation
		$button_line	= qq|
<td>No suicide..
</td>
|;
	    }
	    else
	    {
		if ( $privilege == 0 )
		{
		    $button_line	= qq|
<td><input type="radio" name="decision" value="user_approve"
checked>&nbsp;Approve</td>
|;
		}
		else
		{
		    $button_line	= qq|
<td><input type="radio" name="decision" value="user_delete"
checked>&nbsp;DELETE</td>
|;
		}
	    }

	    $inspector	=~ s/-/N\/A/;
	    $inspector	=  $inspector || "none";

	    $body	.= qq|
<br>
<b>Name:</b> $name<br>
<b>Email:</b> $email<br>
<b>Privilege:</b> $priv_txt<br>
<b>Inspector:</b> $inspector<br>
<table width="100%" border="0">
<tr>
<form action="$cgi_admin" method="POST">
$button_line
<td><input type="hidden" name="action"   value="do_user_modify">
<td><input type="hidden" name="my_email" value="$my_email">
<td><input type="hidden" name="my_priv"  value="$my_priv">
<td><input type="hidden" name="id"       value="$id">
<input type="submit" value="Submit"></td>
</form>
</tr>
</table>
<br>
<hr>
|;
	}
    }

    $sth->finish;

    &disconnect($dbh);

    my $heading	= qq|<font size="+1"><u><b>Found &nbsp; $num_users &nbsp;
                     database users:</b></u></font>|;

    return ($heading, $body);
}

##
# Setting of Browser-Cookie
sub setCookie
{
    my ($my_priv,
	$my_email,
	@cookies)	= @_;

    my $cookie_out	= "Set-Cookie: ";

    foreach (@cookies)
    {
    	$cookie_out	.= $_;
    }

    $cookie_out		.= "\n";
    if ( $my_priv )
    {
	# Do a refresh to fetch a packet from the browser to verify cookie

	my $command	 = "do_pend_tlist&my_email=$my_email&my_priv=$my_priv";
	$cookie_out	.= "Location: $cgi_admin?action=$command\n";
    }

    # Add the main key to displaying the page :-)
    $cookie_out	.= $header;

    # Give the browser the cookie
    print $cookie_out;

#    print "<br>YO - (setCookie) - $cookie_out";
}

##
# Get Cookie back - security check
sub getCookie
{
    my %cookies;

    my @rawCookies	= split ( /; /, $ENV{'HTTP_COOKIE'} );

    foreach (@rawCookies)
    {
	($key, $val)	= split (/=/, $_);
	$cookies{$key}	= $val;
    } 

    return (%cookies);
}

##
# Authenticate (query user/password) & attain user's privilege
sub authenticate
{
    my $dbh		= &connect();

    my $password	= crypt($in{'password'}, $in{'password'});
    $password		= $dbh->quote($password);
    my $email		= $dbh->quote($in{'email'});

    my $sql		= qq|
	SELECT privilege
	FROM   $dict_users
	WHERE  email = $email AND password = $password
|;
    my $sth		= $dbh->prepare_cached($sql) or
	&err("Can't execute search query", $DBI::errstr);

    $sth->execute;

    # Grab the privilege from db if it exists
    my $priv	= ( ($sth->rows eq "0") ? 0 : $sth->fetchrow_array() );

    $sth->finish;
    &disconnect($dbh);

    return($priv);
}

##
# Print error messages
sub err
{
    my $error	= shift;
    my $reason	= shift;

    my $body	= qq|
<center>
<b>$error</b>
<br>
$reason
</center>
|;
    my $heading	= qq|<font color="red" size="+1"><u><b>ERROR:</b></u></font>|;

    &admin_display($in{'my_priv'}, $heading, $body);
    
    # If we're embedding into a site - include these site specific files
    if ($embed)
    {
	&display_file(0, $file_right);
	&display_file(0, $file_footer);
    }

    exit(1);
}

##
# Return Time
sub mygmtime
{
    my ($etime)	= @_;
    my @months	= ("Jan","Feb","Mar","Apr","May","Jun","Jul",
		   "Aug","Sep","Oct","Nov","Dec");
    my @days	= ("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
    my ($sec,
	$min,
	$hr,
	$mday,
	$mon,
	$yr,
	$wday,
	$yday,
	$isdst)	= gmtime($etime);
    my  $timestr	= sprintf("%3s, %02d-%3s-%4d %02d:%02d:%02d GMT",
				  $days[$wday],$mday,$months[$mon],$yr+1900,
				  $hr,$min,$sec);
    return($timestr);
}

1;     # for require (if any)
