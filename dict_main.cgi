#!/usr/bin/perl -T
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Dictionary's main search/submit page.
#
#  + Search for approved dictionary terms
#  + Submit new terms for approval
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - see COPYING file)
#---

# Let's make perl happy in terms of taint'ness :-)
$ENV{"PATH"}	= undef;

#use strict;

require DBI;

require "./dict_header.pl";
require "./dict_lib.pl";

print $header;

if ($embed)
{
    &display_file($title_main, $file_header);
}

my ($ref_in,
    $ignored)	= &readform;

my %in		= %{$ref_in};

if	( $in{'action'} eq "do_search_dict" )	{ &do_search_dict; }
elsif	( $in{'action'} eq "search_dict" )	{ &search_dict(\$cgi_main,
							       \%in); }
elsif	( $in{'action'} eq "do_add_term" )	{ &do_add_term; }
elsif	( $in{'action'} eq "add_term" )		{ &add_term; }
else
{
    # Get the number of terms within the DICT server to display
    $in{'dictd_num'}	= &dictd_term_num($in{'dictd_num'}, \$cgi_main);

    my $content	= "Please use the form below to query ";
    $content   .= "the DICT server ($in{'dictd_num'}):<br>";
    $content   .= &displaysearchform(1, \$cgi_main); 
    $content   .= "<hr><br>Or use the form below ";
    $content   .= "to modify/suggest entries to the database:";
    $content   .= &displayaddform(\$cgi_main, \%in);

    &display($content);
}

# If we're embedding into a site - include these site specific files
if ($embed)
{
    &display_file(0, $file_right);
    &display_file(0, $file_footer);
}

# Normal exit
exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Display the search form
sub do_search_dict
{
    my $content	= &displaysearchform(1, \$cgi_main);
    &display($content);
}

##
# Show the add form
sub do_add_term
{
    my $content	= &displayaddform(\$cgi_main, \%in);
    &display($content);
}

##
# The actual addition
sub add_term
{
    my $form	= &displayaddform(\$cgi_main, \%in);

    ($english,
     $arabic,
     $latin,
     $email,
     $description)	= &process_term_input(\%in, \$form);

    my $dbh	 = &connect();
    $english	 =  $dbh->quote($english);
    $arabic	 =  $dbh->quote($arabic);
    $latin	 =  $dbh->quote($latin);
    $email	 =  $dbh->quote($email);
    $description =  $dbh->quote($description);

    # See if the term is already in the db
    my $sql	= qq|
	SELECT english
	FROM   $dict_terms
	WHERE  english = $english
|;
    my $sth	= $dbh->prepare_cached($sql) or
	  	  &error("ERROR: Can't execute search query", $dbh->errstr);

    $sth->execute;
    my $rows	= $sth->rows;

    $sth->finish;
    if ( $rows ne "0" )
    { &error("ERROR: The term $english is already in the database") }

    # -------------------
    # Add to the database
    $sql	= qq|
	INSERT INTO $dict_terms
	       (id, english, arabic, latin, email, description, can_use,
                hide, status, inspector)
	VALUES ('', $english, $arabic, $latin, $email, $description, '',
                '', '', '-')
|;

    $dbh->do($sql) or &error("ERROR: Can't execute SQL query", $dbh->errstr);
    &disconnect($dbh);

    my $content	= qq|
<center>
<h3>Thank you\!</h3>
<br>
The following data has been added to the database.<br>
The term will be <b>available if/when its approved</b>.
<br>&nbsp;
<table border>
<tr>
<td align="left">English: </td><td> $english</td>
</tr>
<tr>
<td align="left">Arabic: </td><td> $arabic</td>
</tr>
<tr align="left">
<td align="left">Arabic in Latin letters: </td><td> $latin</td>
</tr>
<tr>
<td align="left">Email: </td><td> $email</td>
</tr>
<tr>
<td align="left">Description: </td><td> $description</td>
</tr>
</table>
</center>
|;

    $content	.= "<br><hr>";
    $content	.= &displayaddform(\$cgi_main, \%in);

    &display($content);
}

1;	# for require (if any)
