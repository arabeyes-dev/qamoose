#---
# $Id$
#
# ------------
# Description:
# ------------
#  Dictionary's header/configuration file.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - see COPYING file)
#---

# Package specific settings (version/home-page)
$myname		= "QaMoose";
$mystring	= "(English to Arabic Dictionary)";
$version	= "2.1";
$homepage	= "http://www.arabeyes.org/project.php?proj=QaMoose";

# Database name
$db_name	= "qamoose";

# db_Table used for terms
$dict_terms	= "dict_terms";

# db_Table used for users
$dict_users	= "dict_users";

# Username/Password to access database
$db_username	= "CHANGE_ME";
$db_password	= "CHANGE_ME";

# Username/Password to create database (if different from user)
$ctrl_username	= "";
$ctrl_password	= "";

# Domain name of dictd server
$dictd_host	= "dict.arabeyes.org";

# Database driver
$driver		= "mysql";

# Domain name of where local dictionary will reside
$dom		= "arabeyes";
$Dom		= "\u$dom\E";
$domain		= "$dom.org";

# Admin superuser db creation info (mail sent to address)
$admin_name	= "Super.Admin";
$admin_email    = "admin\@$domain";
$admin_pass	= "CHANGE_ME";

# Site URL
$url		= "http://www.$domain";

# Main dictionary URL
$cgi_main	= "$url/dict_main.cgi"; 

# Admin script URL
$cgi_admin	= "$url/dict_admin.cgi"; 

# Specify sendmail's location
$sendmail	= "/usr/sbin/sendmail";

# Indicate the minimum password length
$min_pass_len	= "5";

# Indicate the height of an input form
$height		= "30";

# Username used in sending emails
$fromname	= "$Dom $myname";

# Email address used in sending emails
$fromemail	= "support\@$domain";

# Email subject line
$fromsubject_u	= "$myname User Application";
$fromsubject_p	= "$fromname - New password";

# Cookie domain name
$dot_domain	= ".$domain";

# Cookie path (all below browser will send cookie)
$cookie_path	= "/";

# Cookie expiration (beginnig of time - ie. now)
$comp_big_bang	= "Thu, 01-Jan-1970 00:00:00 GMT";

# Specify common header to be used
$doctype	= qq
|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">|;
$header		= "Content-type: text/html; charset=utf-8\n\n";

# Specify local titles & messages to be used in HTML output
$str_login	= "$myname.Admin";
$str_main	= "$Dom - $myname $mystring";
$str_admin	= "$Dom - $myname.Admin";
$title_main	= "title=$str_main";
$title_admin	= "title=$str_admin";
#$page_main	= "<h3>\[$Dom - $myname\]</h3>";
$page_main	= qq|<img src="/images/qamoose.png"
			  alt="\[$Dom - $myname\]"|;

# Specify directory to upload files to
$upload_path    = "/CHANGE_ME/path/path";

# Specify whether to print debug messages (debug mode)
$debug          = 0;

# Specify whether this code is embedded or stand-alone (site specific)
$embed		= 0;

# Specify file names and paths of files to embed
$file_header	= "/www/header.inc.php";
$file_right  	= "/www/right.inc.php";
$file_footer	= "/www/footer.inc.php";

# Specify various support applications
$ASPELL		= "/usr/bin/aspell";

1;   # for require (if any)
