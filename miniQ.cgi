#!/usr/bin/perl -T
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  A mini QaMoose CGI/script that accepts a user entered English
#  term/word and tries to look-up its Arabic translation from a
#  specified server via the 'dictd' protocol.  If the word/term
#  produces no results, it is spell-checked for errors.
#
#  Its interface is minimal and it is meant to be light-weight
#  and easily deployable.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - see COPYING file)
#---

#TODO
# - Include 'regex' option code

# Let's make perl happy in terms of taint'ness :-)
$ENV{"PATH"}	= undef;

require "newgetopt.pl";

use CGI;		# Load CGI	routines/methods
use Net::Dict;		# Load Dict	routines/methods
use Net::Domain;	# Load Domain	routines/methods

# Attain proper automatic revision of this script
$REV            = '$Revision$';
$REV            =~ s/.*:\s+(\S+)\s+.*/$1/;

$DICT_HOST	= "arabeyes.org";
$CLIENT		= "mini.QaMoose";
$ASPELL		= "/usr/bin/aspell";
$URL_HOME       = "http://www.$DICT_HOST";
$URL_WORDLIST   = "http://www.$DICT_HOST/project.php?proj=Wordlist";
$HOSTDOMAIN     = &Net::Domain::hostdomain();

&get_args();

if ( $opt_commandline )
{
    $word = $opt_word;

    # Get rid of any preceding/trailing spaces
    $word =~ s/^\s*(.*\S)\s*$/$1/;
}
else
{
    # CGI stuff

    # Create new CGI object
    $query = new CGI;

    # Dump out the "header" HTML info
    print
	$query->header(-charset		=> "UTF-8"),
	$query->start_html(-title	=> 'mini.QaMoose',
			   -BGCOLOR	=> '#E2EBCC'),
	$query->h1('mini.<a href="http://qamoose.arabeyes.org">QaMoose</a>');

    # Deal with getting the sum of all available terms in dbs
    # - Get any passed-in values (so as not to keep hammering server)
    # - Do explicit call (on startup) to attain number of terms
    $num_terms = ( $query->param() ? $query->param('num_terms') :
		                     &query_db_num($num_terms) );

    # Mangle number for readability
    $num_display = reverse $num_terms;
    $num_display =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;
    $num_display = reverse $num_display;

    &output("[Accessing &nbsp <b>$num_display</b> &nbsp; terms]\n\n",
	    "<font color=\"#660066\">");

    print
	$query->startform,
	"Search term: ",
	$query->textfield(-name		=> 'word',
			  -maxlength	=> 50 ),
	$query->hidden(-name	=> 'num_terms',
		       -default	=> $num_terms),
	$query->p,
#	$query->submit("Translate"),
	$query->endform;

    # In case I need to call myself
    # - print "<a href=\"$myself#table1\">See table 1</a>";
    $myself		= $query->url;
    $myself_state	= $query->self_url;
}

# Deal with the command-line version of script
if ( $opt_commandline)
{
    # Make sure we have some input
    if ( !$word )
    {
	&quit("ERROR: please use -word option \n");
    }

    # Do the actual lookup and spell check based on its output
    if ( &do_lookup($word) )
    {
	&do_spellcheck($word);
    }

    exit(0);
}
# Deal with the CGI version of script
else
{
    my $error = $query->cgi_error;

    if ($error)
    {
	print
	    $query->header(-status => $error),
	    $query->start_html('Problems'),
	    $query->h2('Request not processed'),
	    $query->strong($error);

	# Dump out the "footer" HTML info
	print
	    $query->end_html;

        exit(1);
    }  

     # CGI return values capture
     if ( $query->param() )
     {
         $word = $query->param('word');

         # Get rid of any preceding/trailing spaces
	 $word =~ s/^\s*(.*\S)\s*$/$1/;

	 # Do the actual lookup and spell check based on its output
         if ( &do_lookup($word) )
	 {
	     &do_spellcheck($word);
	 }
     }

    # Dump out the "footer" HTML info
    print
	$query->end_html;
}


       ###        ###
######## Procedures ########
       ###        ###

##
# Query the server to attain the number of available terms within total
sub query_db_num
{
    my ($passed_num_terms) = @_;

    my(
       $dict,
       %info,
       $db,
       $dbinfo,
       @info_lines,
       $info_line,
       $num_terms,
      );
       
    $dict	= Net::Dict->new($DICT_HOST,
			         Port     => 2628,
				 Client   => "$CLIENT [$REV] $host_from"
			        );
    %info	= $dict->dbs();

    if (! $passed_num_terms)
    {
	foreach $db (keys %info)
	{
	    $dbinfo	= $dict->dbInfo($db);
	    @info_lines	= split(/\n/, $dbinfo);
	    foreach $info_line (@info_lines)
	    {
		if ($info_line =~ /^NOTE:\s+\D+contains\s(\d+)\sterms/)
		{
		    $num_terms += $1;
		}
	    }
	}
	return($num_terms);
    }
    else
    {
	return($passed_num_terms);
    }
}

##
# The guts of this script - the 'dictd' lookup apparatus
sub do_lookup
{
    my ($term)	= @_;
    
    my (
	$host_from,
	$dict,
	%info,
	$ref_h,
	$tot,
	%assoc_show,
	$key,
	$ref_duple,
	$db,
	$def,
	$homepage,
	$dictionary,
	$tran,
	@tran_multi,
	$i,
       );

    if ($HOSTDOMAIN)
    {
	$host_from = "($HOSTDOMAIN)";
    }

    $dict	= Net::Dict->new($DICT_HOST,
			         Port     => 2628,
				 Client   => "$CLIENT [$REV] $host_from"
			        );

    # Make sure we have a dictd connection to proceed with
    if (! $dict)
    {
	&output("ERROR: Unable to access dictd database (server down).\n",
		"<font color=\"Red\">");
	&quit("Please try again later...\n");
	return(0);
    }

    %info	= $dict->dbs();
    $ref_h	= $dict->define($term);
    $tot	= $#{$ref_h} + 1;

    $assoc_show{$term}	= $ref_h;

    if (!$tot)
    {
	&output("No translations for '$term' were found - Sorry.\n\n",
		"<font color=\"Red\">");
	return(1);
    }

    foreach $key (sort keys %assoc_show)
    {
	foreach $ref_duple ( @{$assoc_show{$key}})
	{
	    ($db, $def) = @{ $ref_duple };

	    if (!$opt_commandline)
	    {
		$homepage   = qq|<a href="$URL_HOME">Arabeyes</a>|;
		$dictionary = qq|<a href="$URL_WORDLIST">dictionary</a>|;
		$info{$db}  =~ s/Arabeyes/$homepage/;
		$info{$db}  =~ s/dictionary/$dictionary/;
	    }

	    &output("From '$db' ($info{$db})\n", "<i>");

	    $tran = $def;
	    $tran =~ s/^$key\s*//i;
	    $tran =~ s/[\n]*$//i;

	    # The '::' is a translation delimiter (for multi-translations)
	    @tran_multi	= split (/\s*\:\:\s*/, $tran);

	    &output("English : $key\n", "<font color=\"Red\">");

	    # Print-out multiple translations
	    # - there is at least a single (0) term
	    for($i=0; $i<=$#tran_multi; $i++)
	    {
		&output("Arabic  : $tran_multi[$i]\n",
			"<font color=\"Green\">");
	    }
	}
    }
    return(0);
}

##
# Do a term/word spell check to ensure proper english word was entered
sub do_spellcheck
{
    my ($term)	= @_;

    my (
	$option,
	$out,
	@lines,
	$line,
	@term_sug,
	@sug,
	$suggest,
	%suggestions,
	$key,
	@new_vals,
       );

    # Although this is not a proper way to untaint things,
    # this is 100% safe, as I'm simply echo'ing the variable
    $term =~ /(.*)/;
    $term = $1;

    my $option	= "-a";

    if (-e $ASPELL)
    {
	$out	= `echo $term | $ASPELL $option`;
    }
    else
    {
	return(1);
    }

    # See if the spell-checker produced any output
    @have_sug	= split(/\:/, $out);

    if ($have_sug[1])
    {
	# Process spell-checker's output line-by-line
	@lines	= split(/\n/, $out);

	foreach $line (@lines)
	{
	    if ($line =~ /\:/)
	    {
		&output("The word you entered isn't in the dictionary.\n");
		&output("Try some of the suggestions noted below.\n\n");

		&output("Suggestions for '$term' :\n",
			"<font color=\"Blue\">");
		@term_sug	= split(/\:\s*/, $line);
		@sug		= split(/\,\s*/, $term_sug[1]);

		# Grab all the unique suggestions (skip multiples)
		foreach $suggest (@sug)
		{
		    $suggestions{uc($suggest)} = $suggest;
		}

		# Dump out the suggestions
		foreach $key (sort keys %suggestions)
		{
		    if ($opt_commandline)
		    {
			&output(" - $suggestions{$key}\n");
		    }
		    else
		    {
			push(@new_vals, $suggestions{$key});
		    }
		}

		# The CGI gets to be a bit fancy in how it prints things out
		if (!$opt_commandline)
		{
		    print
			$query->p,
			$query->startform,
			$query->popup_menu(-name	=> 'word',
					   -values	=> [@new_vals]),
			$query->hidden(-name		=> 'num_terms',
				       -default		=> $num_terms),
#			$query->scrolling_list(-name	=> 'word',
#					       -values	=> [@new_vals],
#					       -size	=> 8),
			$query->submit(-name => "Submit"),
			$query->endform;
		}
	    }
	}
    }
    return(0);
}

##
# Print wrapper to account for script mode (commandline vs. CGI)
sub output
{
    my ($str,
	$tag)	= @_;

    my ($tag_end);

    if ($opt_commandline)
    {
	print $str;
    }
    else
    {
	# The CGI part
	$str =~ s/\n/<br>/g;

	# Was a second argument passed to process ?
	if ($tag)
	{
	    $tag_end = $tag;
	    $tag_end =~ /<(\w*)\s*(.*)*>/;
	    $tag_end = "<\/$1>";
	    print $tag.$str.$tag_end;
	}
	else
	{
	    print $str;
	}
    }
}

##
# Note error message and quit
sub quit
{
    my ($str) = @_;

    if ( $opt_commandline )
    {
	die "$str";
    }
    else
    {
	&output("$str",	"<font color=\"Red\">");
    }
}

##
# Get the command line arguments
sub get_args
{
    &NGetOpt(
             "commandline",		# Run it in non-CGI mode
             "word=s",		        # specify word to lookup
             "regex",			# indicate doing regex
            ) || ( $? = 257, die "Invalid argument\n" );
}
