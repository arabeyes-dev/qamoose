#!/usr/bin/perl -T
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Dictionary's mysql table creation.
#
#  + Create a terms table
#  + Create a users table as well as "the" main user
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - see COPYING file)
#---

require DBI;

require "./dict_header.pl";
require "./dict_lib.pl";

my $dbh	= &connect($ctrl_username, $ctrl_password);
my $sql;

print "Creating TERMS table in $db_name...\n";

# To delete, do it by hand via 'drop table TABLE_NAME'
$sql	= qq|
CREATE TABLE $dict_terms
(
    id          int     (10) unsigned NOT NULL auto_increment,
    english     varchar (250) NOT NULL default '',
    arabic      varchar (250) binary NOT NULL default '',
    latin       varchar (250) NOT NULL default '',
    email       varchar (250) NOT NULL default '',
    description text          NOT NULL,
    hide        tinyint (1)   NOT NULL default '0',
    can_use     tinyint (1)   NOT NULL default '0',
    status      varchar (20)  NOT NULL default '',
    inspector   varchar (65)  NOT NULL default '-',
    downloaded  tinyint (1)   NOT NULL default '0',
    PRIMARY KEY (english),
    KEY id (id, english)
)
|;

$dbh->do($sql) or die "Can't execute SQL query - $DBI::errstr";

print " -> done.\n\n";
print "Creating USERS table in $db_name...\n";

$sql	= qq|
CREATE TABLE $dict_users
(
  id        tinyint (3) unsigned NOT NULL auto_increment,
  name      varchar (100) NOT NULL default '',
  email     varchar (65)  NOT NULL default '',
  password  varchar (50)  NOT NULL default '',
  privilege tinyint (1)   NOT NULL default '0',
  inspector varchar (65)  NOT NULL default '-',
  setting   varchar (65)           default '',
  KEY id(id)
)
|;

$dbh->do($sql) or die "Can't execute SQL query - $DBI::errstr";

print " -> done.\n\n";

# Create super-admin
my $admin_name_q	= $dbh->quote($admin_name);
my $admin_email_q	= $dbh->quote($admin_email);

my $admin_passc		= crypt($admin_pass, $admin_pass);
my $admin_passc_q	= $dbh->quote($admin_passc);

print "Creating USER     : $admin_name\n";
print "         email    : $admin_email\n";
print "         password : $admin_pass\n";

my $sql	= qq|
    INSERT INTO $dict_users
                (id, name, email, password, privilege, inspector)
    VALUES      ('', $admin_name_q, $admin_email_q, $admin_passc_q, '1', '')
|;

$dbh->do($sql) or die "ERROR: Can't execute SQL query - $DBI::errstr";

print " -> done.\n";

&disconnect($dbh);

1;   # for require (if any)
